import React, { useState, useEffect } from'react';

const App = () => {
    const [liked, setLiked] = useState(false);
    const [buttonText, setButtonText] = useState('나만의 페이지 만들기');

    useEffect = () => {
        if (liked) {
            setLiked(false);
        }
        else {
            setLiked(true);
        }
        console.log(liked);
        setButtonText(liked? "나만의 페이지 만들기" : "너만의 페이지 만들기");
    };

    return (
        <div className="App">
            <header>
                <img src="https://dummyimage.com/256x64/7f7f7f/ffffff.jpg?text=logo" className="App-logo" alt="logo" />
            </header>
            <main>
                <button type="button" className="success" onClick={useEffect}>{buttonText}</button>
            </main>
        </div>
    );
}

export default App;